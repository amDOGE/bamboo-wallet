import { StyleSheet, SafeAreaView, View, Text, Button, TouchableOpacity, Image, _View } from "react-native";
import LinearGradient from "react-native-linear-gradient";
import colors from './assets/colors/colors';
import QuestionSvg from './assets/imgs/question.svg'
import HamburgerSvg from './assets/imgs/hamburger.svg'
import SendWalletSvg from './assets/imgs/wallet_send.svg'
import ReceiveWalletSvg from './assets/imgs/wallet_receive.svg'
import SendIconSvg from './assets/imgs/send.svg'
import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createNativeStackNavigator } from '@react-navigation/native-stack';


const Stack = createNativeStackNavigator;

function MyStack() {
  return (
    <Stack.Navigator initialRouteName = 'Main'>
      <Stack.Screen name='Main' component={MainScreen} />
    </Stack.Navigator>
  );
}

function MainScreen() {
  return (
    <LinearGradient colors={[colors.backgroundGradientTop, colors.backgroundGradientBottom]} style={styles.linearGradientStyle}>
      
      <View>
        <SafeAreaView>
          <View style={styles.headerStyle}>
            <TouchableOpacity>
              <QuestionSvg width={40} height={40} style={styles.headerItemStyle} />
            </TouchableOpacity>
            <TouchableOpacity>
              <HamburgerSvg width={40} height={40} style={styles.headerItemStyle} />
            </TouchableOpacity>
          </View>
        </SafeAreaView>
      </View>

      <View style={styles.centerStyle}>
        <Text style={styles.pndTickerStyle}>PND</Text>
        <Text style={styles.pndBalanceStyle}>1,000.00</Text>
      </View>

      <View style={styles.footerStyle}>
        <View style={styles.footerWalletStyle}>
          <TouchableOpacity>
            <SendWalletSvg style={styles.footerItemStyle} />
          </TouchableOpacity>
          <Text style={styles.footerTextStyle}>Send</Text>
        </View>
        <View style={styles.footerWalletStyle}>
          <TouchableOpacity>
            <ReceiveWalletSvg style={styles.footerItemStyle} />
          </TouchableOpacity>
          <Text style={styles.footerTextStyle}>Receive</Text>
        </View>
      </View>
      
    </LinearGradient>
  );
}

export default function App() {
  return (
    <NavigationContainer>
      <MyStack/>
    </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  linearGradientStyle: {
    flex: 1
  },

  headerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 20,
    paddingTop: 20,
    alignItems: 'center'
  },

  headerItemStyle: {
    width: 40,
    height: 40,
    resizeMode: 'contain'
  },

  centerStyle: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },

  pndTickerStyle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 32,
    color: colors.pndTickerColor,
  },

  pndBalanceStyle: {
    fontFamily: 'Montserrat-Regular',
    fontSize: 48,
    color: 'white'
  },

  footerStyle: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    paddingBottom: 40,
    alignItems: 'flex-end'
  },

  footerWalletStyle: {
    alignItems: 'center'
  },

  footerItemStyle: {
    width: 80,
    height: 80,
    resizeMode: 'contain'
  },

  footerTextStyle: {
    color: 'white',
    fontFamily: 'Montserrat-Regular',
    letterSpacing: 0.2,
    paddingTop: 16,
    paddingRight: 2,
    fontSize: 18
  },


});