const colors = {
    backgroundColor: '#141414',
    primaryTextColor: '#f0f8ff',

    backgroundGradientTop: 'rgba(56, 239, 125, 0.74)',
    backgroundGradientBottom: 'rgba(0, 205, 172, 1)',

    pndTickerColor: 'rgba(255, 255, 255, 0.75)'
}

export default colors;